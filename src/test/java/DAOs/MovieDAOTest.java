/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import DTO.Movie;
import java.util.List;
import static jdk.nashorn.internal.objects.NativeRegExp.test;
import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author ed123
 */
public class MovieDAOTest {
    
    private static MovieDAO test;
    
   @BeforeClass
    public static void beforeClass(){
        System.out.println("Before class");
        test = new MovieDAO();
    }
    
    @Before
    public void setup(){
        System.out.println("Before test");
    }
   
    @Test
    public void test(){
        System.out.println("Test executed");
    }
    
    @After
    public void teardown(){
        System.out.println("After test");
    }
    
    @AfterClass
    public static void afterClass(){
        System.out.println("After class");
    }

    /**
     * Test of findAllMovies method, of class MovieDAO.
     */
    @Test
    public void testFindAllMovies() throws Exception {
        System.out.println("findAllMovies");
        MovieDAO instance = new MovieDAO();
        List<Movie> movies = null;
        test.findAllMovies();
       // List<Movie> actual = instance.findAllMovies();
        //assertNotNull(expected);
//        for(Movie actual: expected){
//            instance.findAllMovies();
//            System.out.println(actual.getId());
//        }
         //instance.findAllMovies();
       // assertEquals(expected, actual);
       // fail("The test case is a prototype");
    }

    /**
     * Test of FindMovieByGenre method, of class MovieDAO.
     */
    @Test
    public void testFindMovieByGenre() throws Exception {
        System.out.println("FindMovieByGenre");
        String genre = "Action";
        String genre2 = "Adventure";
        String genre3 = "Disney";
//        MovieDAO instance = new MovieDAO();
//        Movie expected = new Movie(106, "Atlantis: Milo''s Return", "Disney,Adventure, Animation, Family", "Victor Cook, Toby Shelton, Tad Stones", "70 min", "Milo and Kida reunite with their friends to investigate strange occurances around the world that seem to have links to the secrets of Atlantis.", "", null, "G", "DVD", "2003", "James Arnold Taylor, Cree Summer, John Mahoney, Jacqueline Obradors", 1, "5017180887067", null);
//        List<Movie> actual = instance.FindMovieByGenre(genre, genre2, genre3);
        //assertEquals(expected, actual);
        List<Movie> movies = null;
        test.FindMovieByGenre(genre, genre2, genre3);
    }

    /**
     * Test of findMovieByTitle method, of class MovieDAO.
     */
    @org.junit.Test
    public void testFindMovieByTitle() throws Exception {
        System.out.println("findMovieByTitle");
//        String title = "Atlantis: Milo''s Return";
//        Movie expected = new Movie(106, "Atlantis: Milo''s Return", "Disney,Adventure, Animation, Family", "Victor Cook, Toby Shelton, Tad Stones", "70 min", "Milo and Kida reunite with their friends to investigate strange occurances around the world that seem to have links to the secrets of Atlantis.", "", null, "G", "DVD", "2003", "James Arnold Taylor, Cree Summer, John Mahoney, Jacqueline Obradors", 1, "5017180887067", null);
//        Movie actual = test.findMovieByTitle("Atlantis: Milo''s Return");
//        assertEquals(expected, actual);
        //assertEquals("iron man", test.findMovieByTitle(title));
        List<Movie> movies = null;
        test.findMovieByTitle("iron man");
    }

    /**
     * Test of addMovie method, of class MovieDAO.
     */
    @org.junit.Test
    public void testAddMovie() throws Exception {
        System.out.println("addMovie");
        int id = 12;
        String Title = "revenge";
        String Genre = "action";
        String Director = "xxx";
        String Runtime = "2hour58min";
        String Plot = "xxxxxxxxxxxxxxxx";
        String Location = "American";
        String Poster = null;
        String Rating = "PG-13";
        String Format = "xxx";
        int Year = 2019;
        String Starring = "Donie";
        int Copies = 3;
        String Barcode = "5646416";
        String User_rating = "9.8";
        MovieDAO instance = new MovieDAO();
        Movie expected = null;
        Movie actual = instance.addMovie(id, Title, Genre, Director, Runtime, Plot, Location, Poster, Rating, Format, Year, Starring, Copies, Barcode, User_rating);
        assertEquals(expected, actual);
        
    }

    /**
     * Test of deleteMovieByTitle method, of class MovieDAO.
     */
   @org.junit.Test
    public void testDeleteMovieByTitle() throws Exception {
        System.out.println("deleteMovieByTitle");
        String title = "iron man";
        List<Movie> movies = null;
        test.deleteMovieByTitle(title);
        
    }

    /**
     * Test of deleteMovieByID method, of class MovieDAO.
     */
   @org.junit.Test
    public void testDeleteMovieByID() throws Exception {
        System.out.println("deleteMovieByID");
        int ID = 12;
        Movie expected = null;
        Movie actual = test.deleteMovieByID(ID);
        assertEquals(expected, actual);
      
    }

    /**
     * Test of countMovie method, of class MovieDAO.
     */
    @Ignore
    public void testCountMovie() throws Exception {
        System.out.println("countMovie");
        MovieDAO instance = new MovieDAO();
        int expected = 24;
        Movie actual = instance.countMovie();
        assertEquals(expected, actual);
      
    }

    /**
     * Test of updateMovie method, of class MovieDAO.
     */
    @org.junit.Test
    public void testUpdateMovie() throws Exception {
        System.out.println("updateMovie");
        String Title = "Iron man and spider man";
        String Genre = "Action, Adventure";
        String Director = "xxxx";
        int id = 15;
        Movie expected = null;
        Movie actual = test.updateMovie(Title, Genre, Director, id);
        assertEquals(expected, actual);
        
    }
    
}
