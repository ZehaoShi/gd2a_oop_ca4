/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadServer;

import DTO.Movie;
import java.util.List;
import static jdk.nashorn.internal.objects.NativeRegExp.test;
import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author ed123
 */
public class StreamServerTest {
   static StreamServer test;
    @BeforeClass
    public static void beforeClass(){
        System.out.println("Before class");
    }
    
    @Before
    public void setup(){
        System.out.println("Before test");
    }
    
    @Test
    public void test(){
        System.out.println("Test executed");
    }
    
    @After
    public void teardown(){
        System.out.println("After test");
    }
    
    @AfterClass
    public static void afterClass(){
        System.out.println("After class");
    }

    /**
     * Test of receiveFromClient method, of class StreamServer.
     */
    @org.junit.Test
    public void testReceiveFromClient() {
        System.out.println("receiveFromClient");
        StreamServer.receiveFromClient();
    }

    /**
     * Test of JudgeUserRequest method, of class StreamServer.
     */
    @org.junit.Test
    public void testJudgeUserRequest() {
         System.out.println("JudgeUserRequest");
        String userRequest = "recommandation";
        String expected = "";
        String actual = StreamServer.JudgeUserRequest(userRequest);
        assertEquals(expected, actual);
    }

    /**
     * Test of AddMovie method, of class StreamServer.
     */
   @org.junit.Test
    public void testAddMovie() throws Exception {
        System.out.println("AddMovie");
        List<Movie> movies = null;
        for(int i =0; i <10; i ++){
            Movie movie = new Movie();
            movie.setId(i+100);
            movie.setTitle("iron man 2" + i);
            movie.setGenre("action");
            movie.setDirector("xxx");
            movie.setLocation("American");
            movie.setYear("2018");
            movie.setRating("7.2");
            StreamServer.AddMovie(movies);
        }
        System.out.println("Add test movie");
    }

    /**
     * Test of DeleteMovie method, of class StreamServer.
     */
    @org.junit.Test
    public void testDeleteMovie() throws Exception {
         System.out.println("DeleteMovie");
        List<Movie> movies = null;
        int movieID = 1;
        StreamServer.DeleteMovie(movies, movieID);
    }

    /**
     * Test of EditMovieInfo method, of class StreamServer.
     */
    @org.junit.Test
    public void testEditMovieInfo() throws Exception {
        System.out.println("EditMovieInfo");
        Movie movie = new Movie();
        List<Movie> movies = null;
        movie.setId(101);
        movie.setGenre("Advance");
        movie.setYear("2013");
        StreamServer.EditMovieInfo(movies,10);
        
    }

    /**
     * Test of findMovieByTitle method, of class StreamServer.
     */
    @org.junit.Test
    public void testFindMovieByTitle() throws Exception {
        System.out.println("findMovieByTitle");
        List<Movie> movies = null;
        String title = "Iron man 2";
        JSONArray expResult = null;
        JSONArray actual = StreamServer.findMovieByTitle(movies, title);
        assertEquals(expResult, actual);
        
    }

    /**
     * Test of FindMovieByGenre method, of class StreamServer.
     */
   @org.junit.Test
    public void testFindMovieByGenre() throws Exception {
        System.out.println("FindMovieByGenre");
        List<Movie> movies = null;
        String genre = "Action";
        String genre2 = "comedy";
        String genre3 = "Advance";
        JSONArray expResult = null;
        JSONArray actual = StreamServer.FindMovieByGenre(movies, genre, genre2, genre3);
        assertEquals(expResult, actual);
        
    }

    /**
     * Test of ResetSearchCache method, of class StreamServer.
     */
    @org.junit.Test
    public void testResetSearchCache() throws Exception {
        System.out.println("ResetSearchCache");
        List<Movie> movies = null;
        StreamServer.ResetSearchCache(movies);
       
    }
    
}
