/*
 * In TestCase, read testfile in a loop.
    only can show the test success or fail
    
 */
package test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author ed123
 */
@RunWith(Parameterized.class)
public class TestCase {
    
   private File testfile;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Parameters
    public static Collection<Object[]> getFiles(){
        Collection<Object[]> params = new ArrayList<Object[]>();
        //read testfile
        for(File f: new File(".").listFiles()){
            Object[] arr = new Object[]{ f };
            params.add(arr);
        }
        return params;
    }
    
    public TestCase(File testfile){
        this.testfile = testfile;
    }
    
    @Test
    public void test(){
        //achiece test
        assertTrue(!"testCase.txt".equals(testfile.getName()));
    }
    
}
