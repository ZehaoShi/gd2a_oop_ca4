/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadServer;

import Core.ServiceDetails;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author GRonnie
 */
public class ServiceThread extends Thread{
    //Create some variables to hold Socket streams, number of thread
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;
    
    public ServiceThread(ThreadGroup group, String name, Socket dataSocket, int number){
        super(group, name);
        
        try{
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true); // true--> autoflush
            
        }catch(IOException io){
            io.printStackTrace();
        }
    
    }

    @Override
    public void run(){
        //Set up variables for communication
        String incomingMessage = "";
        String response;
        
        try{
            //while the client doesn't want to end the session
            while(!incomingMessage.equals(ServiceDetails.ENDSESSION)){
                response = null;
                //Take the input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);
                
                //Tokenize the input
                String[] components = incomingMessage.split(ServiceDetails.BREAKINGCHARACTER);
                
                //Process the information from the client
                if(components[0].equals(ServiceDetails.ECHO)){
                    StringBuffer echoMessage = new StringBuffer("");
                    
                    if(components.length > 1){
                        echoMessage.append(components[1]);
                    }
                    response = echoMessage.toString();
                }
                else if(components[0].equals(ServiceDetails.DAYTIME)){
                    response = new Date().toString();
                }
                else if(components[0].equals(ServiceDetails.ENDSESSION)){
                    response = ServiceDetails.SESSIONTERMINATED;
                }
                else{
                    
                }
                
                output.println(response);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
            dataSocket.close();
            }catch(IOException io){
                io.printStackTrace();
            }
        }
    }
}
