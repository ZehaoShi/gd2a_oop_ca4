/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadServer;

import Core.ServiceDetails;
import DAOs.MovieDAO;
import DAOs.MovieDAOInterface;
import DTO.Movie;
import Exceptions.DAOException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author GRonnie
 */
public class StreamServer {

    private ServerSocket serverSocket;
    private int listenPort;
    private int maxConnections;

    public StreamServer(int listenPort, int maxConnections) {
        this.listenPort = listenPort;
        this.maxConnections = maxConnections;
    }

    public static void main(String[] args) throws IOException {
        MovieDAOInterface IMovieDAO = new MovieDAO();
        ServerSocket listeningSocket = new ServerSocket(ServiceDetails.CLIENTPORT);
        try {
            while(true){
                List<Movie> movies = IMovieDAO.findAllMovies();
                ProcessClientRequest(movies, listeningSocket);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

    }

    public static void sendToClient(JSONArray obj, String date) {
        try {
            //Create a listening socket
            ServerSocket listenSocket = new ServerSocket(ServiceDetails.SERVERPORT);

            //Create a ThreadGroup to store all clients together
            ThreadGroup group = new ThreadGroup("Client threads");
            //Place more emphasis on accepting threads than on processing threads
            group.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //Wait for incoming connections
            Socket dataSocket = listenSocket.accept();

            //Get an output stream to write to the data socket
            OutputStream outStream = dataSocket.getOutputStream();
            PrintWriter socketOutput = new PrintWriter(new OutputStreamWriter(outStream));
            socketOutput.println(obj + date);

            //Flush stream - ensure that the data is actually written to the socket's stream
            socketOutput.flush();

            listenSocket.close();

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public static void AutorespondToClient(boolean userValidate, String date) {
        if(userValidate == true){
            String respond = "Request Complete!";
            try {
                //Create a listening socket
                ServerSocket listenSocket = new ServerSocket(ServiceDetails.SERVERPORT);

                //Create a ThreadGroup to store all clients together
                ThreadGroup group = new ThreadGroup("Client threads");
                //Place more emphasis on accepting threads than on processing threads
                group.setMaxPriority(Thread.currentThread().getPriority() - 1);

                //Wait for incoming connections
                Socket dataSocket = listenSocket.accept();

                //Get an output stream to write to the data socket
                OutputStream outStream = dataSocket.getOutputStream();
                PrintWriter socketOutput = new PrintWriter(new OutputStreamWriter(outStream));
                socketOutput.println(respond + date);

                //Flush stream - ensure that the data is actually written to the socket's stream
                socketOutput.flush();

                listenSocket.close();

            } catch (IOException io) {
                io.printStackTrace();
            }
        }else{
            String respond = "User is not matching!";
            try {
                //Create a listening socket
                ServerSocket listenSocket = new ServerSocket(ServiceDetails.SERVERPORT);

                //Create a ThreadGroup to store all clients together
                ThreadGroup group = new ThreadGroup("Client threads");
                //Place more emphasis on accepting threads than on processing threads
                group.setMaxPriority(Thread.currentThread().getPriority() - 1);

                //Wait for incoming connections
                Socket dataSocket = listenSocket.accept();

                //Get an output stream to write to the data socket
                OutputStream outStream = dataSocket.getOutputStream();
                PrintWriter socketOutput = new PrintWriter(new OutputStreamWriter(outStream));
                socketOutput.println(respond + date);

                //Flush stream - ensure that the data is actually written to the socket's stream
                socketOutput.flush();

                listenSocket.close();

            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }

    public static void receiveFromClient() {
        String hostname = "127.0.0.1";
        int portNumber = ServiceDetails.CLIENTPORT;
        try {
            InetAddress acceptorHost = InetAddress.getByName(hostname);
            //Try to create a data socket connected to the host
            //Will fail if no one is listening
            Socket requestorSocket = new Socket(acceptorHost, portNumber);
            //Get an input stream to read from the data socket
            InputStream inStream = requestorSocket.getInputStream();
            //Create a scanner to read input from the stream
            Scanner socketInput = new Scanner(new InputStreamReader(inStream));

            //Wait for a message from the acceptor and print is when is arrives
            String userRequest = socketInput.nextLine();
            String date = socketInput.nextLine();
            String userID = socketInput.nextLine();
            int movieID = socketInput.nextInt();
            int frequency = socketInput.nextInt();

            System.out.println("Message received: ");
            System.out.println("User Request: " + userRequest + "\n" + date + "\nUsername: " + userID + "\nMovie ID: " + movieID + "\nFrequency: " + frequency);

            //Message has been received, close the datasocket
            requestorSocket.close();
        } catch (UnknownHostException uhx) {
            uhx.printStackTrace();
        } catch (IOException iox) {
            iox.printStackTrace();
        }
    }

    public static void ProcessClientRequest(List<Movie> movies, ServerSocket listeningSocket) throws DAOException, IOException {

        
        String hostname = "127.0.0.1";
        JSONArray result;
        int portNumber = ServiceDetails.CLIENTPORT;
        try {
            InetAddress acceptorHost = InetAddress.getByName(hostname);
            //Try to create a data socket connected to the host
            //Will fail if no one is listening
            Socket requestorSocket =listeningSocket.accept();
            //Get an input stream to read from the data socket
            InputStream inStream = requestorSocket.getInputStream();
            //Create a scanner to read input from the stream
            Scanner socketInput = new Scanner(new InputStreamReader(inStream));

            //Wait for a message from the acceptor and print is when is arrives
            String userRequest = socketInput.nextLine();
            String titleoutput = socketInput.nextLine();
            String date = socketInput.nextLine();
            String userID = socketInput.nextLine();
            int movieID = socketInput.nextInt();
            int frequency = socketInput.nextInt();
            String genre = socketInput.nextLine();
            String genre2 = socketInput.nextLine();
            String emptyNextLine = socketInput.nextLine();
            String genre3 = socketInput.nextLine();
            String password = socketInput.nextLine();
            String newMovieName = socketInput.nextLine();
            String newGenre = socketInput.nextLine();
            String newDirector = socketInput.nextLine();
            
            
            //User Login Validate
            boolean userValidate = true;
            System.out.println(date);
            if(!userID.matches("user") && !password.matches("123456")){
                userValidate = false;
            }

            if(userValidate == true){
                if (null != userRequest) {
                    switch (userRequest) {
                        case "add":
                            AddMovie(movies);
                            AutorespondToClient(userValidate, date);
                            break;
                        case "update":
                            EditMovieInfo(movies, movieID, newMovieName, newGenre, newDirector);
                            ResetSearchCache(movies);
                            AutorespondToClient(userValidate, date);
                            break;
                        case "delete":
                            DeleteMovie(movies, movieID);
                            AutorespondToClient(userValidate, date);
                            break;
                        case "search":
                            result = findMovieByTitle(movies, titleoutput);
                            sendToClient(result, date);
                            break;
                        case "recommendation":
                            result = FindMovieByGenre(movies, genre, genre2, genre3);
                            sendToClient(result, date);

                            break;
                        default:
                            break;
                    }
                }
            }else{
                AutorespondToClient(userValidate, date);
            }

            //Message has been received, close the datasocket
            requestorSocket.close();
        } catch (UnknownHostException uhx) {
            uhx.printStackTrace();
        } catch (IOException iox) {
            iox.printStackTrace();
        }

    }

    public static String JudgeUserRequest(String userRequest) {
        String result = null;
        String pattern = "^watchmovie$";
        String pattern2 = "^watch movie$";

        if (Pattern.matches(pattern, userRequest) || Pattern.matches(pattern2, userRequest)) {
            result = "watchmovie";
            return result;
        }
        return result;
    }

    public static void DisplayAllMovies(List<Movie> movies) throws DAOException {
        JSONArray JsonMovie = new JSONArray();
        if (movies.isEmpty()) {
            System.out.println("There are no movies");
        }
        for (int i = 0; i < movies.size(); i++) {
            JsonMovie.put(movies.get(i));
        }
        System.out.println(JsonMovie);
    }

    public static void AddMovie(List<Movie> movies) throws DAOException {

        MovieDAOInterface IMovieDAO = new MovieDAO();
        Movie movie = IMovieDAO.addMovie(java.sql.Types.INTEGER, "Crazy ET", "Comedy",
                "Ninghao", "102 min", "A story about the process ET how to make a relationship with human",
                "", "", "N/A", "DVD", 2019, "huangbo,shenteng", 1, "5037456431", "7.8");   // https://stackoverflow.com/questions/14514589/inserting-null-to-an-integer-column-using-jdbc    set id to null
        System.out.println("Movie added complete");
    }

    public static void DeleteMovie(List<Movie> movies, int movieID) throws DAOException {
        try {
            MovieDAOInterface IMovieDAO = new MovieDAO();
            Movie movie = IMovieDAO.deleteMovieByID(movieID);
            System.out.println("Movie has been deleted!");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void EditMovieInfo(List<Movie> movies, int movieID, String newMovieName, String newGenre, String newDirector) throws DAOException {
        try {
            MovieDAOInterface IMovieDAO = new MovieDAO();
            Movie movie = IMovieDAO.updateMovie(newMovieName , newGenre, newDirector, movieID);
            System.out.println("Movie has updated");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONArray findMovieByTitle(List<Movie> movies, String title) throws DAOException {
        MovieDAOInterface IMovieDAO = new MovieDAO();
        Movie movie = IMovieDAO.findMovieByTitle(title);
        JSONArray JsonMovie = new JSONArray();

        JsonMovie.put(movie);

        List<Movie> FoundedMovieName = new ArrayList<>();
        FoundedMovieName.add(movie);

        if (movie != null) {
            System.out.println("Movie found: " + JsonMovie);
            SaveSearchCache(FoundedMovieName);
        } else {
            System.out.println("Movie was not found");
        }
        return JsonMovie;
    }

    public static JSONArray FindMovieByGenre(List<Movie> movies, String genre, String genre2, String genre3) throws DAOException {
        MovieDAOInterface IMovieDAO = new MovieDAO();
        List<Movie> movie = IMovieDAO.FindMovieByGenre(genre, genre2, genre3);
        JSONArray JsonMovie = new JSONArray();

        JsonMovie.put(movie);

        if (movie != null) {
            System.out.println("Movie found: " + JsonMovie);
        } else {
            System.out.println("Movie was not found");
        }
        return JsonMovie;
    }

    public static void SaveSearchCache(List<Movie> movies) throws DAOException {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(new FileWriter("Cache.txt"));
            outputStream.write("{\"Movies\" : [");

            ListIterator<Movie> iter = movies.listIterator();
            while (iter.hasNext()) {
                outputStream.write(iter.next().toJSONObject());

                if (iter.hasNext()) {
                    outputStream.write(",");
                }
            }
            outputStream.write("]}");
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
        System.out.println("Output Completed!");
    }

    public static void ResetSearchCache(List<Movie> movies) throws DAOException {
        PrintWriter outputStream = null;
        String output = null;
        try {
            FileOutputStream fileout = new FileOutputStream("Cache.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(output);
            out.close();
            fileout.close();
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
        System.out.println("Cache has been reset!");
    }

}
