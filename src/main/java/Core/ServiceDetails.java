/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

/**
 *
 * @author GRonnie
 */
public class ServiceDetails {
    public static final int SERVERPORT = 8888;
    public static final int CLIENTPORT = 8999;
    public static final int MAXCONNECTIONS = 10;
    
    public static final String BREAKINGCHARACTER = "%%";
    
    //Command Strings
    public static final String ENDSESSION = "QUIT";
    public static final String ECHO = "ECHO";
    public static final String DAYTIME = "DAYTIME";
    
    //Response 
    public static final String UNRECONGNISED = "UNKNOWNCOMMAND";
    public static final String SESSIONTERMINATED = "GOODBYE";
}
