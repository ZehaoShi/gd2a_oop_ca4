/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainAPP;

import Core.ServiceDetails;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author GRonnie
 */
public class Client {

    public static void main(String[] args) throws IOException {
        Scanner ss = new Scanner(System.in);
        int option;
        try {
            do {
                MainMenu();
                option = ss.nextInt();
                switch (option) {
                    case 0:
                        System.out.println("Exiting...");
                        break;

                    case 1:
                        SendToServer();
                        receiveFromServer();
                        break;

                    case 2:
                        LoadCache();
                        break;
                }
            } while (option != 0);
        } catch (NullPointerException e) {
            System.out.println("NullPointerException caught");
        } catch (InputMismatchException e2) {
            System.out.println("Exception caught");
        }
    }

    public static void MainMenu() {
        System.out.println("1. Send Request to Server");
        System.out.println("2. Load Cache");
        System.out.println("0. Exit");
    }

    public static void receiveFromServer() {
        String hostname = "127.0.0.1";
        int portNumber = ServiceDetails.SERVERPORT;

        try {
            InetAddress acceptorHost = InetAddress.getByName(hostname);
            //Try to create a data socket connected to the host
            //Will fail if no one is listening
            Socket requestorSocket = new Socket(acceptorHost, portNumber);

            //Get an input stream to read from the data socket
            InputStream inStream = requestorSocket.getInputStream();
            //Create a scanner to read input from the stream
            Scanner socketInput = new Scanner(new InputStreamReader(inStream));

            //Wait for a message from the acceptor and print is when is arrives
            String message = socketInput.nextLine();
            System.out.println(message);

            //Message has been received, close the datasocket
            requestorSocket.close();
        } catch (UnknownHostException uhx) {
            uhx.printStackTrace();
        } catch (IOException iox) {
            iox.printStackTrace();
        }
    }

    public static void SendToServer() {
        Scanner sc = new Scanner(System.in);
        Scanner sd = new Scanner(System.in);
        String userRequest;
        int requestOption;
        String userID = null;
        int movieID = 0;
        int frequency = 0;
        String title = null;
        String titleoutput;
        String genre = null;
        String genre2 = null;
        String genre3 = null;
        String password;
        String newMovieName = null;
        String newGenre = null;
        String newDirector = null;
        int option;

        System.out.println("1. Add movie");
        System.out.println("2. Update movie");
        System.out.println("3. Delete movie");
        System.out.println("4. Search movie");
        System.out.println("5. Recommendation Movie");
        System.out.print("Select your request(Number): ");
        requestOption = sc.nextInt();

        switch (requestOption) {
            case 1:
                userRequest = "add";
                break;
            case 2:
                userRequest = "update";
                System.out.print("Enter movie ID: ");
                movieID = sc.nextInt();
                System.out.println("Enter new Movie name: ");
                newMovieName = sd.nextLine();
                System.out.println("Enter new Genre: ");
                newGenre = sd.nextLine();
                System.out.println("Enter new Director: ");
                newDirector = sd.nextLine();
                break;
            case 3:
                userRequest = "delete";
                System.out.print("Enter movie ID: ");
                movieID = sc.nextInt();
                break;
            case 4:
                userRequest = "search";
                System.out.print("Search movie name: ");
                title = sd.nextLine();
                break;
            case 5:
                userRequest = "recommendation";
                System.out.println("Select 3 Movie Genre you like.(Enter 0 to exit)");
                System.out.println("1. Comedy");
                System.out.println("2. Crime");
                System.out.println("3. Adventure");
                System.out.println("4. Romance");
                System.out.println("5. Family");
                System.out.println("6. Biograph");
                System.out.println("7. Sport");
                for (int i = 1; i <= 3; i++) {
                    System.out.println("Select Genre " + i);
                    option = sc.nextInt();
                    if (i == 1) {
                        if (option == 1) {
                            genre = "Comedy";
                        } else if (option == 2) {
                            genre = "Crime";
                        } else if (option == 3) {
                            genre = "Adventure";
                        } else if (option == 4) {
                            genre = "Romance";
                        } else if (option == 5) {
                            genre = "Family";
                        } else if (option == 6) {
                            genre = "Biograph";
                        } else if (option == 7) {
                            genre = "Sport";
                        } else if (option == 0) {
                            break;
                        } else {
                            System.out.println("Wrong input");
                            break;
                        }
                    }  
                    if (i == 2) {
                        if (option == 1) {
                            genre2 = "Comedy";
                        } else if (option == 2) {
                            genre2 = "Crime";
                        } else if (option == 3) {
                            genre2 = "Adventure";
                        } else if (option == 4) {
                            genre2 = "Romance";
                        } else if (option == 5) {
                            genre2 = "Family";
                        } else if (option == 6) {
                            genre2 = "Biograph";
                        } else if (option == 7) {
                            genre2 = "Sport";
                        } else if (option == 0) {
                            break;
                        } else {
                            System.out.println("Wrong input");
                            break;
                        }
                    }
                    if (i == 3) {
                        if (option == 1) {
                            genre3 = "Comedy";
                        } else if (option == 2) {
                            genre3 = "Crime";
                        } else if (option == 3) {
                            genre3 = "Adventure";
                        } else if (option == 4) {
                            genre3 = "Romance";
                        } else if (option == 5) {
                            genre3 = "Family";
                        } else if (option == 6) {
                            genre3 = "Biograph";
                        } else if (option == 7) {
                            genre3 = "Sport";
                        } else if (option == 0) {
                            break;
                        } else {
                            System.out.println("Wrong input");
                            break;
                        }
                    }
                }
                break;
            default:
                userRequest = null;
                break;
        }

        System.out.println("Default username: user");
        System.out.println("Default password: 123456");
        System.out.print("Enter your username: ");
        userID = sd.nextLine();
        System.out.print("Enter your password: ");
        password = sd.nextLine();
        titleoutput = "%" + title + "%";
        System.out.println("Request submit!");

        try {
            //Do main logic of the server
            //Wait for incoming connections
            Socket dataSocket = new Socket("127.0.0.1", ServiceDetails.CLIENTPORT);

            //Get an output stream to write to the data socket
            OutputStream outStream = dataSocket.getOutputStream();
            PrintWriter socketOutput = new PrintWriter(new OutputStreamWriter(outStream));

            //send date
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String stringDate = dateFormat.format(date);

            frequency++;
            socketOutput.println(userRequest + "\n" + titleoutput + "\n" + stringDate + "\n" + userID + "\n" + movieID + "\n" + frequency + "\n" + genre + "\n" + genre2 + "\n" + genre3 + "\n" + password + "\n" + newMovieName + "\n" + newGenre + "\n" + newDirector);
            //Flush stream - ensure that the data is actually written to the socket's stream
            socketOutput.flush();

            dataSocket.close();

        } catch (IOException io) {
            io.printStackTrace();
        }

    }

    public static void LoadCache() throws IOException {
        File file = new File("Cache.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            System.out.println(st);
        }
    }

}
