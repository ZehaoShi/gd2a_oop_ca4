/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author GRonnie
 */
public class Movie implements Serializable{
    private static final long serialVersionUID = 1L;
    private int id;
    private String title;
    private String genre;
    private String director;
    private String runtime;
    private String plot;
    private String location;
    private String poster;
    private String rating;
    private String format;
    private String year;
    private String starring;
    private int copies;
    private String barcode;
    private String user_rating;

    public Movie(int id, String title, String genre, String director, String runtime, String plot, String location, String poster, String rating, String format, String year, String starring, int copies, String barcode, String user_rating) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.poster = poster;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.user_rating = user_rating;
    }
    
    public Movie(String title, String genre, String director, String runtime, String plot, String location, String poster, String rating, String format, String year, String starring, int copies, String barcode, String user_rating) {
        this.id = 0;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.poster = poster;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.user_rating = user_rating;
    }

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getLocation() {
        return location;
    }

    public String getPoster() {
        return poster;
    }

    public String getRating() {
        return rating;
    }

    public String getFormat() {
        return format;
    }

    public String getYear() {
        return year;
    }

    public String getStarring() {
        return starring;
    }

    public int getCopies() {
        return copies;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }
    
    

    @Override
    public String toString() {
        return "Movie{" + "id=" + id + ", title=" + title + ", genre=" + genre + ", director=" + director + ", runtime=" + runtime + ", plot=" + plot + ", location=" + location + ", poster=" + poster + ", rating=" + rating + ", format=" + format + ", year=" + year + ", starring=" + starring + ", copies=" + copies + ", barcode=" + barcode + ", user_rating=" + user_rating + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.title);
        hash = 71 * hash + Objects.hashCode(this.genre);
        hash = 71 * hash + Objects.hashCode(this.director);
        hash = 71 * hash + Objects.hashCode(this.runtime);
        hash = 71 * hash + Objects.hashCode(this.plot);
        hash = 71 * hash + Objects.hashCode(this.location);
        hash = 71 * hash + Objects.hashCode(this.poster);
        hash = 71 * hash + Objects.hashCode(this.rating);
        hash = 71 * hash + Objects.hashCode(this.format);
        hash = 71 * hash + Objects.hashCode(this.year);
        hash = 71 * hash + Objects.hashCode(this.starring);
        hash = 71 * hash + this.copies;
        hash = 71 * hash + Objects.hashCode(this.barcode);
        hash = 71 * hash + Objects.hashCode(this.user_rating);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.copies != other.copies) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.genre, other.genre)) {
            return false;
        }
        if (!Objects.equals(this.director, other.director)) {
            return false;
        }
        if (!Objects.equals(this.runtime, other.runtime)) {
            return false;
        }
        if (!Objects.equals(this.plot, other.plot)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.poster, other.poster)) {
            return false;
        }
        if (!Objects.equals(this.rating, other.rating)) {
            return false;
        }
        if (!Objects.equals(this.format, other.format)) {
            return false;
        }
        if (!Objects.equals(this.year, other.year)) {
            return false;
        }
        if (!Objects.equals(this.starring, other.starring)) {
            return false;
        }
        if (!Objects.equals(this.barcode, other.barcode)) {
            return false;
        }
        if (!Objects.equals(this.user_rating, other.user_rating)) {
            return false;
        }
        return true;
    }
    
    
    /*************************************
     * Serialization
     * @return 
     */

    /*
    Builds a String as a JSON object representing the song.
    @return String containing the JSON data
     */
    public String toJSONObject(){
        return "{" + "\"id\":" + "\"" + this.id + "\"," + "\"title\":" + "\"" + this.title +"\"," + "\"," + "\"genre\":" + "\"" + this.genre + "\"director\":" + "\"" + this.director +
                "\"runtime\":" + "\"" + this.runtime + "\"plot\":" + "\"" + this.plot + "\"location\":" + "\"" + this.location + "\"poster\":" + "\"" + this.poster 
                + "\"rating\":" + "\"" + this.rating + "\"format\":" + "\"" + this.format + "\"year\":" + "\"" + this.year + "\"starring\":" + "\"" + this.starring + "\"copies\":" + "\"" + this.copies
                + "\"barcode\":" + "\"" + this.barcode + "\"user_rating\":" + "\"" + this.user_rating +"}";
    }

    public String toCSV(){
        return this.id + "," + this.title + "," + this.genre + "," + this.director + "," + this.runtime + "," + this.plot + "," + this.location + "," + this.poster + "," + this.rating + "," + this.format 
                + "," + this.year + "," + this.starring + "," + this.copies + "," + this.barcode + "," + this.user_rating;
    }

    public String toHTMLTableData(){
        return "<td>" + this.id + "</td>" +
                "<td>" + this.title + "</td>" +
                "<td>" + this.genre + "</td>" +
                "<td>" + this.director + "</td>" + 
                "<td>" + this.runtime + "</td>" +
                "<td>" + this.plot + "</td>" +
                "<td>" + this.location + "</td>" +
                "<td>" + this.poster + "</td>" +
                "<td>" + this.rating + "</td>" +
                "<td>" + this.format + "</td>" +
                "<td>" + this.year + "</td>" +
                "<td>" + this.starring + "</td>" +
                "<td>" + this.copies + "</td>" +
                "<td>" + this.barcode + "</td>" +
                "<td>" + this.user_rating + "</td>" ;

    }

    public String toXML(){
        return "<id>" + this.id + "</id>" +
                "<title>" + this.title + "</title>" +
                "<genre>" + this.genre + "</genre>" +
                "<director>" + this.director + "</director>" +
                "<runtime>" + this.runtime + "</runtime>" +
                "<plot>" + this.plot + "</plot>" +
                "<location>" + this.location + "</location>" +
                "<poster>" + this.poster + "</poster>" +
                "<rating>" + this.rating + "</rating>" +
                "<format>" + this.format + "</format>" +
                "<year>" + this.year + "</year>" +
                "<starring>" + this.starring + "</starring>" +
                "<copies>" + this.copies + "</copies>" +
                "<barcode>" + this.barcode + "</barcode>" +
                "<user_rating>" + this.user_rating + "</user_rating>";
    }
}
