/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

/**
 *
 * @author GRonnie
 */
import DTO.Movie;
import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MovieDAO extends MysqlDAO implements MovieDAOInterface{
    
    @Override
    public List<Movie> findAllMovies() throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Movie> movies = new ArrayList<>();

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT * FROM movies";
            ps = con.prepareStatement(query);

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            while(rs.next()){
                int id = rs.getInt("ID");
                String title = rs.getString("TITLE");
                String genre = rs.getString("GENRE");
                String director = rs.getString("DIRECTOR");
                String runtime = rs.getString("RUNTIME");
                String plot = rs.getString("PLOT");
                String location = rs.getString("LOCATION");
                String poster = rs.getString("POSTER");
                String rating = rs.getString("RATING");
                String format = rs.getString("FORMAT");
                String year = rs.getString("YEAR");
                String starring = rs.getString("STARRING");
                int copies = rs.getInt("COPIES");
                String barcode = rs.getString("BARCODE");
                String user_rating = rs.getString("USER_RATING");

                Movie newMovie = new Movie(id, title, genre, director, runtime, plot, location, poster, rating, format, year, starring, copies, barcode, user_rating);
                movies.add(newMovie);
            }

        }catch(SQLException e){
            throw new DAOException("findAllMovies() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("findAllMovies() " + e.getMessage());
            }
        }
        return movies;
    }

    @Override
    public List<Movie> FindMovieByGenre(String genre, String genre2, String genre3) throws DAOException{ 
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;
        List<Movie> movies = new ArrayList<>();
        try{
            //Get connection to the DB
            con = this.getConnection();
            if(genre2 == null && genre3 == null){
                String query = "SELECT * FROM movies WHERE genre in (?)";
                ps = con.prepareStatement(query);
                ps.setString(1, genre);
            }
            else if(genre3 == null){
                String query = "SELECT * FROM movies WHERE genreibn (?, ?)";
                ps = con.prepareStatement(query);
                ps.setString(1, genre);
                ps.setString(2, genre2);
            }
            else{
                String query = "SELECT * FROM movies WHERE genre in (?, ?, ?)";
                ps = con.prepareStatement(query);
                ps.setString(1, genre);
                ps.setString(2, genre2);
                ps.setString(3, genre3);
            }

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            while(rs.next()){
                int id = rs.getInt("ID");
                String title = rs.getString("TITLE");
                genre = rs.getString("GENRE");
                String director = rs.getString("DIRECTOR");
                String runtime = rs.getString("RUNTIME");
                String plot = null;
                String location = rs.getString("LOCATION");
                String poster = rs.getString("POSTER");
                String rating = rs.getString("RATING");
                String format = rs.getString("FORMAT");
                String year = rs.getString("YEAR");
                String starring = rs.getString("STARRING");
                int copies = rs.getInt("COPIES");
                String barcode = rs.getString("BARCODE");
                String user_rating = rs.getString("USER_RATING");

                newMovie = new Movie(id, title, genre, director, runtime, plot, location, poster, rating, format, year, starring, copies, barcode, user_rating);
                movies.add(newMovie);
            }

        }catch(SQLException e){
            throw new DAOException("FindMovieByGenre() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("FindMovieByGenre() " + e.getMessage());
            }
        }
        return movies;
    }
    
   @Override
    public Movie findMovieByTitle(String title) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT * FROM movies WHERE title like ?";
            ps = con.prepareStatement(query);
            ps.setString(1, title);

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            if(rs.next()){
                int id = rs.getInt("ID");
                title = rs.getString("TITLE");
                String genre = rs.getString("GENRE");
                String director = rs.getString("DIRECTOR");
                String runtime = rs.getString("RUNTIME");
                String plot = null;
                String location = rs.getString("LOCATION");
                String poster = rs.getString("POSTER");
                String rating = rs.getString("RATING");
                String format = rs.getString("FORMAT");
                String year = rs.getString("YEAR");
                String starring = rs.getString("STARRING");
                int copies = rs.getInt("COPIES");
                String barcode = rs.getString("BARCODE");
                String user_rating = rs.getString("USER_RATING");

                newMovie = new Movie(id, title, genre, director, runtime, plot, location, poster, rating, format, year, starring, copies, barcode, user_rating);
            }

        }catch(SQLException e){
            throw new DAOException("findMovieByTitle() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("findMovieByTitle() " + e.getMessage());
            }
        }
        return newMovie;
    }
    
    @Override
    public Movie addMovie(int id, String Title, String Genre, String Director, String Runtime, String Plot,
            String Location, String Poster, String Rating,String Format, int Year,
            String Starring, int Copies, String Barcode, String User_rating) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "INSERT INTO movies VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(query);
            ps.setNull(1, java.sql.Types.INTEGER);  // https://stackoverflow.com/questions/14514589/inserting-null-to-an-integer-column-using-jdbc    set id to null
            ps.setString(2,Title);
            ps.setString(3, Genre);
            ps.setString(4, Director);
            ps.setString(5, Runtime);
            ps.setString(6, Plot);
            ps.setString(7, Location);
            ps.setString(8, Poster);
            ps.setString(9, Rating);
            ps.setString(10,Format);
            ps.setInt(11,Year);
            ps.setString(12,Starring);
            ps.setInt(13, Copies);
            ps.setString(14, Barcode);
            ps.setString(15, User_rating);
            
            //Use the prepared statement to execute SQL
            ps.executeUpdate();
            
        }catch(SQLException e){
            throw new DAOException("addMovie() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("addMovie() " + e.getMessage());
            }
        }
        return newMovie;
    }
    
     @Override
    public Movie deleteMovieByTitle(String title) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "delete FROM movies WHERE title = ?";
            
            ps = con.prepareStatement(query);
            ps.setString(1 , title);
            ps.executeUpdate();
        }catch(SQLException e){
            throw new DAOException("deleteMovieByTitle() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("deleteMovieByTitle() " + e.getMessage());
            }catch(NullPointerException f) {
                throw new DAOException("deleteMovieByTitle() " + f.getMessage());
            }
        }
        return newMovie;
    }
    
    @Override
    public Movie deleteMovieByID(int ID) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "delete FROM movies WHERE id = ?";
            
            ps = con.prepareStatement(query);
            ps.setInt(1 , ID);
            ps.executeUpdate();
        }catch(SQLException e){
            throw new DAOException("deleteMovieByID() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("deleteMovieByID() " + e.getMessage());
            }catch(NullPointerException f) {
                throw new DAOException("deleteMovieByID() " + f.getMessage());
            }
        }
        return newMovie;
    }
    
     @Override
    public Movie countMovie() throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT COUNT(id) FROM movies";
            ps = con.prepareStatement(query);

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            if(rs.next()){
                int id = rs.getInt("ID");
                String title = rs.getString("TITLE");
                String genre = rs.getString("GENRE");
                String director = rs.getString("DIRECTOR");
                String runtime = rs.getString("RUNTIME");
                String plot = rs.getString("PLOT");
                String location = rs.getString("LOCATION");
                String poster = rs.getString("POSTER");
                String rating = rs.getString("RATING");
                String format = rs.getString("FORMAT");
                String year = rs.getString("YEAR");
                String starring = rs.getString("STARRING");
                int copies = rs.getInt("COPIES");
                String barcode = rs.getString("BARCODE");
                String user_rating = rs.getString("USER_RATING");

                newMovie = new Movie(id, title, genre, director, runtime, plot, location, poster, rating, format, year, starring, copies, barcode, user_rating);
            }

        }catch(SQLException e){
            throw new DAOException("countMovie() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("countMovie() " + e.getMessage());
            }
        }
        return newMovie;
    }

     @Override
    public Movie updateMovie(String Title, String Genre, String Director, int id) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie newMovie = null;

        try{
            //Get connection to the DB
            con = this.getConnection();
            String query = "UPDATE movies set title=?, genre=?, director=? WHERE id = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, Title);
            ps.setString(2, Genre);
            ps.setString(3, Director);
            ps.setInt(4, id);
            
            //Use the prepared statement to execute SQL
            ps.executeUpdate();


        }catch(SQLException e){
            throw new DAOException("updateMovie() " + e.getMessage());
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
                if(con != null){
                    this.closeConnection(con);
                }
            }catch(SQLException e){
                throw new DAOException("updateMovie() " + e.getMessage());
            }
            
        }
        return newMovie;
    }




}

