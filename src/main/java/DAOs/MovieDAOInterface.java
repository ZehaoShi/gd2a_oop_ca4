/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import DTO.Movie;
import Exceptions.DAOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author GRonnie
 */
public interface MovieDAOInterface {
    public List<Movie> findAllMovies() throws Exceptions.DAOException;
    public List<Movie> FindMovieByGenre(String genre, String genre2, String genre3) throws DAOException;
    public Movie findMovieByTitle(String title) throws DAOException;
    public Movie addMovie(int id, String Title, String Genre, String Director, String Runtime, String Plot,
            String Location, String Poster, String Rating,String Format, int Year,
            String Starring, int Copies, String Barcode, String User_rating) throws DAOException;
    public Movie deleteMovieByTitle(String title) throws DAOException;
    public Movie deleteMovieByID(int id) throws DAOException;
    public Movie countMovie() throws DAOException;
    public Movie updateMovie(String Title, String Genre, String Director, int id) throws DAOException;
    
}
