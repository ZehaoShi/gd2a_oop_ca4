/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Exceptions.DAOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author GRonnie
 */
public class MysqlDAO {
    public Connection getConnection() throws DAOException
    {
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/movie_list?serverTimezone=UTC";
        String username = "root";
        String password = "";
        Connection con = null;

        try{
            con = DriverManager.getConnection(url, username, password);
        }
        catch(SQLException ex){
            System.out.println("Connection failed " + ex.getMessage());
            System.exit(1);
        }
        return con;
    }

    public void closeConnection(Connection con) throws DAOException{
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        }
        catch(SQLException e){
            System.out.println("Failed to free the connection: " + e.getMessage());
            System.exit(1);
        }
    }
}
