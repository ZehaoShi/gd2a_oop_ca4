/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import java.sql.SQLException;

/**
 *
 * @author GRonnie
 */
public class DAOException extends SQLException {
    public DAOException() {
    }

    public DAOException(String eMessage){
        super(eMessage);
    }
}

